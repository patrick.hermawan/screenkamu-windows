import os
# import tempfile
# import traceback
from random import randint
from time import sleep

import instaloader
import ctypes


L = instaloader.Instaloader()

while True:
    wait_time = 1800
    try:
        posts = instaloader.Profile.from_username(L.context, "screenkamu").get_posts()
        L.download_post(next(posts), "screenkamu_downloads")

        all_files = os.listdir("screenkamu")
        files = [i[0:19] for i in all_files if i.endswith(".jpg")]
        latest_date = sorted(files)[-1]

        latest_picture_files = [i for i in all_files if i.endswith(".jpg") and i.startswith(latest_date)]
        picture_file_to_use = latest_picture_files[randint(0, len(latest_picture_files)-1)]

        absolute_path = os.path.abspath(os.path.join("screenkamu", picture_file_to_use))

        ctypes.windll.user32.SystemParametersInfoW(20, 0, absolute_path, 0)
    except Exception as e:
        # tb = traceback.format_exc()
        # print("ERROR", tb)
        # print(e)
        wait_time = 60
    sleep(wait_time)
